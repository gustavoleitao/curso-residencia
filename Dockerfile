FROM node:14
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN npm ci
EXPOSE 3000
CMD [ "npm", "start" ]
