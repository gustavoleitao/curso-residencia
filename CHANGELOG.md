## [1.1.1](https://gitlab.com/gustavoleitao/curso-residencia/compare/v1.1.0...v1.1.1) (2021-08-06)


### Bug Fixes

* corrige numero de replicas ([70556ff](https://gitlab.com/gustavoleitao/curso-residencia/commit/70556ff898fbcc28f5d7db4d17d950d2a1facf79))

# [1.1.0](https://gitlab.com/gustavoleitao/curso-residencia/compare/v1.0.0...v1.1.0) (2021-07-16)


### Features

* adiciona calculo fatorial ([cd00edc](https://gitlab.com/gustavoleitao/curso-residencia/commit/cd00edc2c23af3334b195945e5ab8edbbe2a412f))

# 1.0.0 (2021-07-16)


### Code Refactoring

* adiciona configuração de comilint ([e9def10](https://gitlab.com/gustavoleitao/curso-residencia/commit/e9def100940ee94600e11b1bd3c5eec21b10efdb))


### Config

* adiciona .gitinore e arquivo de requisições ([6067a57](https://gitlab.com/gustavoleitao/curso-residencia/commit/6067a575733278a87aaaecff853aa4d9797d6377))


### Features

* adiciona fibonnaci function ([9a101bc](https://gitlab.com/gustavoleitao/curso-residencia/commit/9a101bcbf7fc4d53b3066d0cac870114aaff150d))
