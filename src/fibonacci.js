let fibonnaci = function (n) {
    if (n <= 0) return []
    if (n == 1) return [0]
    let sequenceArray = [0, 1];
    for (var i = 2; i <= n; i++) {
       sequenceArray.push(sequenceArray[i-1] + sequenceArray[i-2]);
    }
    return sequenceArray;
}

module.exports = fibonnaci