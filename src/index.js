const express = require('express')
const fibonnaci = require('./fibonacci')
const app = express()
const port = 3000
const pkg = require('../package.json')

app.get('/', (req, res) => {
  res.send(`Hello World from version ${pkg.version}`)
})

app.get('/fibonnaci/:n', (req, res) => {
    const input = req.params.n
    const result = fibonnaci(input)
    res.json({input: input, output: result})
})

const server = app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = server