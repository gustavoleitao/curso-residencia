const chai = require("chai");
const server = require("../src/index");
const chaiHttp = require("chai-http");
const should = chai.should();

chai.use(chaiHttp);

describe("API Test", () => {

  describe("GET /fibonnaci", () => {
    it("GET /fibonnaci/10", (done) => {
      chai
        .request("http://localhost:3000")
        .get("/fibonnaci/10") 
        .end((err, res) => {
          res.should.have.status(200)
          done();
        });
    });
  });

  after(function() {
    server.close()
  });

});

