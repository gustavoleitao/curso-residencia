const fibonnaci = require('../src/fibonacci')
const expect = require('chai').expect

describe('Fibonnaci Test', function() {

  it('Deve retornar array vazio', function() {
    const r = fibonnaci(0)
    expect(r).deep.to.equal([]);
  });
  it('Deve retornar array com 1 elemento', function() {
    const r = fibonnaci(1)
    expect(r).deep.to.equal([0]);
  });
  it('Deve retornar sequencia váida', function() {
    const r = fibonnaci(10)
    expect(r).deep.to.equal([0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55]);
  });

});